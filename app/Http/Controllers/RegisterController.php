<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function register(Request $data) {
        $data->validate([
            'name' => ['required', 'string', 'max:255'],
            'movil' => ['required', 'integer'],
            'username' => ['required', 'string', 'max:255'],
            'birthday' => ['required', 'date'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'terminos' => ['accepted'],
            'privacidad' => ['accepted'],
        ]);

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'movil' =>$data['movil'],
            'username' => $data['username'],
            'terminos' => 1,
            'privacidad' => 1,
            'birthday' => $data['birthday'],
            'password' => Hash::make($data['password']),
        ]);

        if (Auth::attempt($data->only('email', 'password'))){
            return response()->json(Auth::user(), 200);
        }
    }
}

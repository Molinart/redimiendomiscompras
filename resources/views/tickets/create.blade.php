@extends('layouts.app')

@section('content')

    <div class="col-md-8">
        <div class="card">
            <div class="card-header">{{ __('Nuevo Ticket') }}</div>

            <div class="card-body">

                @if($errors->any())
                    {!! implode('', $errors->all('<div>:message</div>')) !!}
                @endif

                <form method="POST" action="{{ route('tickets.create') }}">
                    @csrf

                    <div class="form-group row">
                        <label for="monto" class="col-md-4 col-form-label text-md-right">{{ __('monto') }}</label>

                        <div class="col-md-6">
                            <input id="monto" type="text" class="form-control @error('monto') is-invalid @enderror" name="monto" value="{{ old('monto') }}" required autocomplete="monto" autofocus>

                            @error('monto')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="tienda" class="col-md-4 col-form-label text-md-right">{{ __('tienda') }}</label>

                        <div class="col-md-6">
                            <input id="tienda" type="text" class="form-control @error('tienda') is-invalid @enderror" name="tienda" value="{{ old('tienda') }}" required autocomplete="tienda" autofocus>

                            @error('tienda')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="img" class="col-md-4 col-form-label text-md-right">{{ __('img') }}</label>

                        <div class="col-md-6">
                            <input id="img" type="text" class="form-control @error('img') is-invalid @enderror" name="img" value="{{ old('img') }}" required autocomplete="img" autofocus>

                            @error('img')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Register') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ 'Redimiendo mis compras' }} @hasSection('title') | @yield('title') @endif</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- polyfills -->
    <script src="{{ asset('assets/js/vendor/polyfills.min.js') }}"></script>

    <!-- UIKit js -->
    <script src="{{ asset('assets/js/uikit.min.js') }}"></script>
</head>
<body>

<div id="app">
    <header id="sc-header">
        <nav class="uk-navbar uk-navbar-container" data-uk-navbar="mode: click; duration: 360">
            <div class="uk-navbar-left nav-overlay-small uk-margin-right uk-navbar-aside">
                <div v-if="hasHistory()" class="sc-padding-small sc-js-uikit-code sc-el-clickable sc-el-hoverable uk-border-rounded" @click="$router.go(-1)">
                    <span data-uk-icon="icon: chevron-left; ratio: 1.4" class="uk-icon"></span>
                </div>
            </div>
            <div class="nav-overlay nav-overlay-small uk-navbar-right">
                <ul class="uk-navbar-nav">
                    @guest
                        <router-link
                            to="/login"
                            v-slot="{ href, route, navigate, isActive, isExactActive }"
                        >
                            <li>
                                <a :href="href" @click="navigate">Iniciar Sesión</a>
                            </li>
                        </router-link>
                        <router-link
                            to="/register"
                            v-slot="{ href, route, navigate, isActive, isExactActive }"
                        >
                            <li>
                                <a :href="href" @click="navigate">Regístrate</a>
                            </li>
                        </router-link>
                    @else
                    <li>
                        <a href="#"><img src="assets/img/avatars/avatar_default_sm.png" alt=""></a>
                        <div class="uk-navbar-dropdown uk-dropdown-small">
                            <ul class="uk-nav uk-nav-navbar">
                                <router-link
                                    to="/dashboard"
                                    v-slot="{ href, route, navigate, isActive, isExactActive }"
                                >
                                    <li>
                                        <a :href="href" @click="navigate">Dashboard</a>
                                    </li>
                                </router-link>
                                <li><a @click.prevent="logout">Cerrar Sesión</a></li>
                            </ul>
                        </div>
                    </li>
                    @endguest
                </ul>
            </div>
        </nav>
    </header>

    <div id="sc-page-content">
            @yield('content')
    </div>

    @auth
        <footer class="sc-footer sc-theme-bg-dark sc-footer-light" style="width: 100%;left: 0;">
            <div class="uk-flex uk-visible@m">
                <div class="uk-flex-1">
                    <div class="uk-grid uk-grid-divider uk-child-width-auto uk-grid-medium uk-visible@m" data-uk-grid>
                        <router-link to="/terminos-y-condiciones">Términos y Condiciones</router-link>
                        <router-link to="/politicas-de-privacidad">Políticas de Privacidad</router-link>
                    </div>
                </div>
                <div>
                    Redimiendo mis compras
                </div>
            </div>
            <div class="uk-grid uk-child-width-expand uk-flex-center uk-margin-remove-top uk-grid-collapse uk-height-1-1 uk-hidden@m" data-uk-grid>
                <router-link to="/terminos-y-condiciones">Términos y Condiciones</router-link>
                <router-link to="/politicas-de-privacidad">Políticas de Privacidad</router-link>
            </div>
            <div class="sc-fab-wrapper uk-hidden@m">
                <router-link
                    to="/dashboard"
                    v-slot="{ href, route, navigate, isActive, isExactActive }"
                    class="sc-fab sc-fab-secondary"
                >
                        <a :href="href" @click="navigate"><i class="mdi mdi-plus"></i></a>
                </router-link>
                <div class="round-corner left"></div>
                <div class="round-corner right"></div>
            </div>
        </footer>
    @endauth
</div>

    <!-- async assets-->
    <script src="{{ asset('assets/js/vendor/loadjs.min.js') }}"></script>
    <script>
        var html = document.getElementsByTagName('html')[0];
        // ----------- CSS
        // md icons
        loadjs('assets/css/materialdesignicons.min.css', {
            preload: true
        });
        // UIkit
        loadjs('node_modules/uikit/dist/css/uikit.min.css', {
            preload: true
        });
        // themes
        loadjs('assets/css/themes/themes_combined.min.css', {
            preload: true
        });
        // mdi icons (base64) & google fonts (base64)
        loadjs([
            'assets/css/fonts/mdi_fonts.css',
            'assets/css/fonts/roboto_base64.css',
            'assets/css/fonts/sourceCodePro_base64.css'
        ], {
            preload: true
        });
        // main stylesheet
        loadjs('assets/css/main.min.css', function() {});
        // vendor
        loadjs('assets/js/vendor.min.js', function () {
            // scutum common functions/helpers
            loadjs('assets/js/scutum_common.min.js', function() {
                scutum.init();

                // show page
                setTimeout(function () {
                    // clear styles (FOUC)
                    $(html).css({
                        'backgroundColor': '',
                    });
                    $('body').css({
                        'visibility': '',
                        'overflow': '',
                        'apacity': '',
                        'maxHeight': ''
                    });
                }, 100);
                // style switcher
                loadjs([
                    'assets/js/style_switcher.min.js',
                    'assets/css/plugins/style_switcher.min.css'
                ], {
                    success: function() {
                        $(function(){
                            scutum.styleSwitcher();
                        });
                    }
                });
            });
        });
    </script>
</body>
</html>

<div class="list-group">
    <a href="{{ route('home') }}" class="list-group-item list-group-item-action @if(Route::current()->getName() == 'home') active @endif">Mis Tickets</a>
    <a href="{{ route('tickets.create') }}" class="list-group-item list-group-item-action">Nuevo Ticket</a>
</div>

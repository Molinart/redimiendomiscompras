@extends('layouts.app')
@section('title', 'Términos y condiciones')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Términos y Condiciones') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate dolor id odio quae rem. Ad, at cum cupiditate dolore harum minima nulla quo repellat. Autem doloribus expedita modi nihil saepe?
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

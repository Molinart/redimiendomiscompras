@extends('layouts.app')
@section('title', 'Políticas de privacidad')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Políticas de privacidad') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aut dolorem facere incidunt iusto laudantium neque porro provident quam repellendus. Exercitationem hic maxime minus mollitia nam nihil non, officia perspiciatis.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

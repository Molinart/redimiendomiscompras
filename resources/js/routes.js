import Home from './components/HomeComponent.vue';
import Dashboard from './components/DashboardComponent.vue';
import NotFound from './components/NotFoundComponent';
import Login from './components/LoginComponent';
import Register from './components/RegisterComponent';
import Terminos from './components/Terminos';
import Politicas from './components/Politicas';

export default {
    mode: 'history',
    linkActiveClass: 'uk-text-bold',
    routes: [
        {
            path: '*',
            component: NotFound
        },
        {
            path: '/',
            component: Home,
            name: Home
        },
        {
            path: '/politicas-de-privacidad',
            component: Politicas
        },
        {
            path: '/terminos-y-condiciones',
            component: Terminos
        },
        {
            path: '/login',
            component: Login,
            name: Login,
            beforeEnter: (to, form, next) =>{
                axios.get('/api/athenticated').then(()=>{
                    return next({ name: 'Dashboard'})
                }).catch(()=>{
                    next()
                })
            }
        },
        {
            path: '/register',
            component: Register,
            beforeEnter: (to, form, next) =>{
                axios.get('/api/athenticated').then(()=>{
                    return next({ name: 'Dashboard'})
                }).catch(()=>{
                    next()
                })
            }
        },
        {
            path: "/dashboard",
            name: "Dashboard",
            component: Dashboard,
            beforeEnter: (to, form, next) =>{
                axios.get('/api/athenticated').then(()=>{
                    next()
                }).catch(()=>{
                    return next({ name: 'Login'})
                })
            }

        }
    ],
}
